package handlers;

import common.Type;

public abstract class Approver {

    protected Approver next;

    /**
     * If needed, be free to change signature of abstract methods.
     */
    public abstract void approve(int id, double cost, Type type);
    protected abstract boolean canApprove(double cost, Type type);  //Protected je jer se poziva samo u nasledjenim klasama, ne i izvan njih/
                                                                    //Dok je f-ja approve public zato sto se koristi i izvan

    /**
     * Method used for registering next approver level.
     * DO NOT CHANGE IT.
     */
    public Approver registerNext(Approver next) {
        this.next = next;
        return next;
    }
}
