import common.Type;
import handlers.Approver;
import handlers.Director;
import handlers.Manager;

/**
 * Execution class of the application.
 * Be free to modify below line 14 (bellow comment line)
 */
public class PurchaseApprovalExecutor {

    public static void execute() {
        Approver manager = new Manager();
        ApprovalChainGenerator.generate(manager);

        Approver direktor= new Director();
        ApprovalChainGenerator.generate(direktor);  //

        direktor.approve(11,1500, Type.GADGETS); //Ovo moze direktor da dozvoli
        direktor.approve(10, 2000, Type.CLERICAL); //President moze da odobri, Direktor ne moze!

        manager.approve(1, 300, Type.CONSUMABLES);
        manager.approve(2, 1000, Type.CLERICAL);
        manager.approve(3, 2000, Type.GADGETS);
        manager.approve(4, 5000, Type.GAMING);
        manager.approve(5, 9000, Type.PC);
    }
}
